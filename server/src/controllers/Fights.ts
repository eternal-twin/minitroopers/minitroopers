import { FightResult, PrismaClient, User } from "@minitroopers/prisma";
import { Request, Response } from "express";
import { auth, IncludeAllUserData } from "../utils/UserHelper.js";
import {
  checkNameValide,
  getFightState,
  parseToPartialUser,
  PartialUserExtended,
  PowerDiff,
  shuffle,
  UserExtended,
} from "@minitroopers/shared";

const Fights = {
  getOpponents:
    (prisma: PrismaClient) => async (req: Request, res: Response) => {
      try {
        const user = await auth(prisma, req);

        if (!user) {
          throw new Error();
        }

        if (!getFightState(user.fights).some((x) => x === "pending")) {
          throw new Error("No fight left");
        }

        const alreadySeens = []; //user.fights.map((x) => x.opponent); // <---------- UNCOMMENT
        alreadySeens.push(user.armyName);

        const takeArgument = 10;

        const users = await prisma.user.findMany({
          where: {
            armyName: {
              notIn: alreadySeens,
            },
            power: {
              gte: user.power - PowerDiff,
              lte: user.power + PowerDiff,
            },
          },
          orderBy: {
            power: "asc",
          },
          take: takeArgument,
          include: {
            troopers: true,
          },
        });

        const opponents = shuffle(users)
          .slice(0, 4)
          .map((x) => parseToPartialUser(x));

        return res.send({ opponents: opponents });
      } catch (error: any) {
        return res.send({ status: "error", reason: error?.message });
      }
    },

  createFight:
    (prisma: PrismaClient) => async (req: Request, res: Response) => {
      try {
        if (
          !req.body.opponentName ||
          typeof req.body.opponentName !== "string" ||
          !checkNameValide(req.body.opponentName)
        ) {
          throw new Error();
        }

        const user = await auth(prisma, req);

        if (!user) {
          throw new Error();
        }

        if (!getFightState(user.fights).some((x) => x === "pending")) {
          throw new Error("No fight left");
        }

        const opponent = await prisma.user.findFirst({
          where: {
            armyName: {
              equals: req.body.opponentName as string,
              mode: "insensitive",
            },
          },
          include: {
            troopers: true,
          },
        });

        if (!opponent) {
          throw new Error();
        }

        if (user.power > opponent.power + PowerDiff * 2) {
          throw new Error(); // power diff + margin too important
        }

        const { returnedUser, fightId } = await generateFight(
          user,
          opponent,
          prisma
        ); // <-----

        return res.send({ user: returnedUser, fightId: fightId });
      } catch (error: any) {
        return res.send({ status: "error", reason: error?.message });
      }
    },
};

const generateFight = async (
  user: UserExtended,
  opponent: Omit<PartialUserExtended, "history" | "fights">,
  prisma: PrismaClient
) => {
  const result: FightResult = Math.random() < 0.5 ? "win" : "lose"; // <----- TO IMPLEMENTE

  const fight = await prisma.fight.create({
    data: {
      userId: user.id,
      opponent: opponent.armyName,
      result: result,
      details: {
        create: {
          actions: [], // <----------- DETAILS GOES HERE
        },
      },
    },
  });

  const returnedUser = await prisma.user.update({
    where: { id: user.id },
    data: {
      gold: {
        increment: result === "win" ? 2 : 1,
      },
      fights: {
        connect: fight,
      },
      history: {
        create: {
          type: "war",
          options: {
            result: result,
            fightId: fight.id,
            enemyName: opponent.armyName,
          },
        },
      },
    },
    include: IncludeAllUserData,
  });

  return { returnedUser: returnedUser, fightId: fight.id };
};

export default Fights;
