import {
  HelmetName,
  ItemName,
  PerkName,
  PrismaClient,
  VehicleName,
  WeaponName,
} from "@minitroopers/prisma";
import { getAddCost, getUpgradeCost, pickSkill } from "@minitroopers/shared";
import { Request, Response } from "express";
import { auth, IncludeAllUserData } from "../utils/UserHelper.js";

const Troopers = {
  upgrade: (prisma: PrismaClient) => async (req: Request, res: Response) => {
    try {
      if (!req.body.trooperId || typeof req.body.trooperId != "string") {
        throw new Error();
      }

      const user = await auth(prisma, req);

      if (!user) {
        throw new Error();
      }

      const trooper = user.troopers.find((x) => x.id === req.body.trooperId);

      if (!trooper) {
        throw new Error();
      }

      if (trooper.savedSill1) {
        res.send();
        return;
      }

      const upgradeCost = getUpgradeCost(trooper.level);

      if (user.gold < upgradeCost) {
        throw new Error("Not enought gold");
      }

      let i: number = 0;

      const skill1 = pickSkill(trooper);
      if (!skill1) {
        throw new Error("Skill not picked");
      }

      let skill2;
      skill2 = pickSkill(trooper, [skill1]);
      if (!skill2) {
        throw new Error("Skill not picked");
      }

      let skill3;
      if (trooper.skills.includes(PerkName.smart)) {
        skill3 = pickSkill(trooper, [skill1, skill2]);
        if (!skill3) {
          throw new Error("Skill not picked");
        }
      }

      await prisma.trooper.update({
        where: {
          id: trooper.id,
        },
        data: {
          savedSill1: skill1,
          savedSill2: skill2,
          savedSill3: skill3,
          level: {
            increment: 1,
          },
        },
      });

      const userUpdated = await prisma.user.update({
        where: {
          id: user.id,
        },
        data: {
          gold: user.gold - upgradeCost,
          power: { increment: 1 },
        },
        include: IncludeAllUserData,
      });

      res.send(userUpdated);
    } catch (error: any) {
      res.send({ status: "error", reason: error?.message });
    }
  },

  chooseSkill:
    (prisma: PrismaClient) => async (req: Request, res: Response) => {
      try {
        if (!req.body.trooperId || typeof req.body.trooperId != "string") {
          throw new Error();
        }
        if (
          !req.body.skillName ||
          typeof req.body.skillName != "string" ||
          ![
            ...Object.values(WeaponName),
            ...Object.values(PerkName),
            ...Object.values(ItemName),
            ...Object.values(VehicleName),
            ...Object.values(HelmetName),
          ].includes(req.body.skillName)
        ) {
          throw new Error();
        }

        const user = await auth(prisma, req);

        if (!user) {
          throw new Error();
        }

        const trooper = user.troopers.find((x) => x.id === req.body.trooperId);

        if (!trooper) {
          throw new Error();
        }

        const skill = req.body.skillName;

        if (skill in WeaponName) {
          await prisma.trooper.update({
            where: {
              id: trooper.id,
            },
            data: {
              savedSill1: null,
              savedSill2: null,
              savedSill3: null,
              weapons: {
                push: skill,
              },
            },
          });
        } else if (skill in ItemName) {
          await prisma.trooper.update({
            where: {
              id: trooper.id,
            },
            data: {
              savedSill1: null,
              savedSill2: null,
              savedSill3: null,
              items: {
                push: skill,
              },
            },
          });
        } else if (skill in HelmetName) {
          await prisma.trooper.update({
            where: {
              id: trooper.id,
            },
            data: {
              savedSill1: null,
              savedSill2: null,
              savedSill3: null,
              helmet: skill,
            },
          });
        } else if (skill in VehicleName) {
          await prisma.trooper.update({
            where: {
              id: trooper.id,
            },
            data: {
              savedSill1: null,
              savedSill2: null,
              savedSill3: null,
              vehicle: { push: skill },
            },
          });
        } else {
          // Perk
          await prisma.trooper.update({
            where: {
              id: trooper.id,
            },
            data: {
              savedSill1: null,
              savedSill2: null,
              savedSill3: null,
              skills: {
                push: skill,
              },
            },
          });
        }

        const userUpdated = await prisma.user.findFirst({
          where: {
            id: user.id,
          },
          include: IncludeAllUserData,
        });

        res.send(userUpdated);
      } catch (error: any) {
        res.send({ status: "error", reason: error?.message });
      }
    },

  add: (prisma: PrismaClient) => async (req: Request, res: Response) => {
    try {
      if (!req.body.trooper || typeof req.body.trooper != "string") {
        throw new Error();
      }

      const user = await auth(prisma, req);

      if (!user || user.troopers.length > 11) {
        throw new Error("limit troopers");
      }

      const existingTodayTrooper = await prisma.trooperDay.findFirst({
        where: {
          id: req.body.trooper,
        },
      });

      if (!existingTodayTrooper) {
        throw new Error();
      }

      const goldNeeded = getAddCost(user.troopers.length);

      if (!(goldNeeded > 0) || user.gold < goldNeeded) {
        throw new Error("Not enought gold");
      }

      const updatedUser = await prisma.user.update({
        where: { id: user.id },
        data: {
          gold: {
            decrement: goldNeeded,
          },
          power: {
            increment: 5,
          },
          troopers: {
            create: {
              name: existingTodayTrooper.name,
              items: existingTodayTrooper.items,
              skills: existingTodayTrooper.skills,
              weapons: existingTodayTrooper.weapons,
              col0: existingTodayTrooper.col0,
              col1: existingTodayTrooper.col1,
              col2: existingTodayTrooper.col2,
              p0: existingTodayTrooper.p0,
              p1: existingTodayTrooper.p1,
            },
          },
        },
        include: IncludeAllUserData,
      });

      res.send(updatedUser);
    } catch (error: any) {
      res.send({ status: "error", reason: error?.message });
    }
  },
};

export default Troopers;
