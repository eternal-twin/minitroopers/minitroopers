import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-container-blue',
  standalone: true,
  imports: [],
  templateUrl: './container-blue.component.html',
  styleUrl: './container-blue.component.scss',
})
export class ContainerBlueComponent {}
