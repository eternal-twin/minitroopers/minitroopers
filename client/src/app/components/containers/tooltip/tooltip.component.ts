import { CommonModule } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { WeaponName } from '@minitroopers/prisma';
import {
  SkillName,
  WeaponAttribute,
  WeaponAttributes,
} from '@minitroopers/shared';
import { GetSkillAttributePipe } from 'src/app/pipes/get-skill-attribute.pipe';

@Component({
  selector: 'app-tooltip',
  standalone: true,
  imports: [CommonModule, GetSkillAttributePipe],
  templateUrl: './tooltip.component.html',
  styleUrl: './tooltip.component.scss',
})
export class TooltipComponent implements OnInit {
  @Input() skillName: SkillName | undefined = undefined;

  tooltipType: 'Weapon' | 'Skill' = 'Skill';

  weaponAttributes: WeaponAttribute[] = WeaponAttributes;

  ngOnInit(): void {
    this.tooltipType =
      this.skillName && this.skillName in WeaponName ? 'Weapon' : 'Skill';
  }
}
