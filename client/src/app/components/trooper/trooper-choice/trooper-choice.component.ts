import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output, inject } from '@angular/core';
import { Trooper } from '@minitroopers/prisma';
import { SkillName } from '@minitroopers/shared';
import { take } from 'rxjs';
import { TrooperService } from 'src/app/services/trooper.service';
import { TrooperCellComponent } from '../trooper-cell/trooper-cell.component';

@Component({
  selector: 'app-trooper-choice',
  standalone: true,
  imports: [TrooperCellComponent, CommonModule],
  templateUrl: './trooper-choice.component.html',
  styleUrl: './trooper-choice.component.scss',
})
export class TrooperChoiceComponent {
  @Input() selectedTrooper!: Trooper;
  @Output() returnChoice: EventEmitter<boolean> = new EventEmitter();
  @Output() updatedTrooper: EventEmitter<Trooper> = new EventEmitter();

  private trooperService = inject(TrooperService);

  lock: boolean = false;

  chooseSkill(skill: SkillName) {
    if (!this.lock && this.selectedTrooper) {
      this.lock = true;
      this.trooperService
        .chooseSkill(this.selectedTrooper.id, skill)
        .pipe(take(1))
        .subscribe((response) => {
          if (response.troopers?.length) {
            const trooperUpdated = response.troopers.find(
              (x) => x.id == this.selectedTrooper.id
            );
            if (trooperUpdated) {
              this.updatedTrooper.emit(trooperUpdated);
              this.returnChoice.emit(true);
            }
          }
        });
    }
  }
}
