import { CommonModule } from '@angular/common';
import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { Trooper } from '@minitroopers/prisma';
import { TrooperCellComponent } from '../trooper-cell/trooper-cell.component';

@Component({
  selector: 'app-troopers-grid',
  standalone: true,
  imports: [CommonModule, TrooperCellComponent],
  templateUrl: './troopers-grid.component.html',
  styleUrl: './troopers-grid.component.scss',
})
export class TroopersGridComponent implements OnChanges {
  @Input() troopers: Trooper[] = [];
  troopersToAdd: number[] = [];

  ngOnChanges(changes: SimpleChanges): void {
    let toAdd = 3 - (this.troopers.length % 3);
    if (toAdd == 0) {
      toAdd = 3;
    }
    this.troopersToAdd = Array(toAdd).fill(0);
  }
}
