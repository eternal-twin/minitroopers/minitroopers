import { CommonModule } from '@angular/common';
import { Component, Input, OnInit, inject } from '@angular/core';
import { SkillsComponent } from '../skills/skills.component';
import { Trooper, TrooperDay } from '@minitroopers/prisma';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { SkillName } from '@minitroopers/shared';

@Component({
  selector: 'app-trooper-cell',
  standalone: true,
  imports: [CommonModule, SkillsComponent],
  templateUrl: './trooper-cell.component.html',
  styleUrl: './trooper-cell.component.scss',
})
export class TrooperCellComponent implements OnInit {
  @Input() trooper: TrooperDay | Trooper | null = null;
  @Input() withSkills: boolean = false;
  @Input() isAddTrooper: boolean = false;
  @Input() isUserTrooper: boolean = false;
  @Input() selected: boolean = false;
  @Input() disabledForm: boolean = false;

  firstSkill: SkillName | undefined = undefined;
  secondSkill: SkillName | undefined = undefined;

  private router = inject(Router);
  private route = inject(ActivatedRoute);
  private authService = inject(AuthService);

  ngOnInit(): void {
    if (this.trooper?.weapons.length && this.trooper.weapons.length > 0) {
      this.firstSkill = this.trooper.weapons[0];

      if (this.trooper.weapons.length > 1) {
        this.secondSkill = this.trooper.weapons[1];
      } else if (
        this.trooper?.skills.length &&
        this.trooper.skills.length > 0
      ) {
        this.secondSkill = this.trooper.skills[0];
      } else if (this.trooper?.items.length && this.trooper.items.length > 0) {
        this.secondSkill = this.trooper.items[0];
      }
    } else if (this.trooper?.skills.length && this.trooper.skills.length > 0) {
      this.firstSkill = this.trooper.skills[0];

      if (this.trooper.skills.length > 1) {
        this.secondSkill = this.trooper.skills[1];
      } else if (this.trooper?.items.length && this.trooper.items.length > 0) {
        this.secondSkill = this.trooper.items[0];
      }
    } else if (this.trooper?.items.length && this.trooper.items.length > 0) {
      this.firstSkill = this.trooper.items[0];
      this.secondSkill = this.trooper.items[1];
    }
  }

  trooperClicked(trooper: Trooper | TrooperDay | null) {
    if (
      this.authService.user &&
      this.authService.user?.armyName == this.route.snapshot.params['army']
    ) {
      if (this.isAddTrooper) {
        this.router.navigate([this.authService.user.armyName + '/add']);
      } else if (trooper) {
        this.router.navigate([this.authService.user.armyName + '/edit'], {
          state: { trooper: trooper.id },
        });
      }
    }
  }
}
