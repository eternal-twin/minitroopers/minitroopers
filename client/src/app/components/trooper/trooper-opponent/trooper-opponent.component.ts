import { CommonModule } from '@angular/common';
import { Component, EventEmitter, inject, Input, Output } from '@angular/core';
import { PartialUserExtended } from '@minitroopers/shared';
import { TrooperCellComponent } from '../trooper-cell/trooper-cell.component';
import { ArmyNameComponent } from '../../containers/army-name/army-name.component';
import { AuthService } from 'src/app/services/auth.service';
import { FightService } from 'src/app/services/fight.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-trooper-opponent',
  standalone: true,
  imports: [TrooperCellComponent, CommonModule, ArmyNameComponent],
  templateUrl: './trooper-opponent.component.html',
  styleUrl: './trooper-opponent.component.scss',
})
export class TrooperOpponentComponent {
  @Input() opponent!: PartialUserExtended;
  @Input() lock: boolean = false;
  @Output() opponentSelected: EventEmitter<string> = new EventEmitter();

  onClick(event: MouseEvent, opponentName: string) {
    if (!this.lock) {
      this.opponentSelected.emit(opponentName);
    }
  }
}
