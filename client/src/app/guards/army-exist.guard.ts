import { CanActivateFn, Router } from '@angular/router';
import { inject } from '@angular/core';
import { BackendService } from '../services/backend.service';
import { map, take } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { checkNameValide } from '@minitroopers/shared';

export const armyExistGuard: CanActivateFn = (route, state) => {
  const backendService = inject(BackendService);
  const router = inject(Router);
  const authService = inject(AuthService);

  if (route.params['army']) {
    if (route.params['army'] == authService.user?.armyName) {
      return true;
    }

    if (!checkNameValide(route.params['army'])) {
      // if (false) {
      return router.parseUrl('/');
    }

    return backendService.checkArmyExist(route.params['army']).pipe(
      take(1),
      map((status) => {
        if (status) {
          return true;
        }
        return router.parseUrl('/');
      })
    );
  } else {
    return router.parseUrl('/');
  }
};
