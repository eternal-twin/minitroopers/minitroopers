import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { PartialUserExtended, UserExtended } from '@minitroopers/shared';
import { map, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root',
})
export class FightService {
  private http = inject(HttpClient);
  private authService = inject(AuthService);

  getOpponents() {
    return this.http
      .get<{ opponents: PartialUserExtended[] }>(
        environment.apiUrl + '/api/fight/getOpponents'
      )
      .pipe(
        map((response) =>
          response?.opponents?.length ? response.opponents : []
        )
      );
  }

  createFight(opponentName: string) {
    return this.http
      .post<{ user: UserExtended; fightId: string }>(
        environment.apiUrl + '/api/fight/createFight',
        {
          opponentName: opponentName,
        }
      )
      .pipe(
        tap((response) => {
          if (response?.user) {
            this.authService.user = response?.user;
          }
        })
      );
  }

  getFightDetails(fightId: string) {
    return this.http.post<{ steps: any }>(
      environment.apiUrl + '/api/fight/getFight',
      {
        fightId: fightId,
      }
    );
  }
}
