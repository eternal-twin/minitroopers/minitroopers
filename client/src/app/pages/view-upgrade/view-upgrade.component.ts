import { CommonModule } from '@angular/common';
import { Component, inject } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { Trooper } from '@minitroopers/prisma';
import { Subject, filter, takeUntil, tap } from 'rxjs';
import { IconContainerComponent } from 'src/app/components/containers/container-icon/container-icon.component';
import { TrooperChoiceComponent } from 'src/app/components/trooper/trooper-choice/trooper-choice.component';
import { TrooperSkillsComponent } from 'src/app/components/trooper/trooper-skills/trooper-skills.component';
import { TroopersBlockComponent } from 'src/app/components/trooper/troopers-block/troopers-block.component';
import { ContainerComponent } from 'src/app/layouts/container/container.component';
import { AuthService } from 'src/app/services/auth.service';
import { PrefixArmy } from '../signup/signup.component';

@Component({
  selector: 'app-view-upgrade',
  standalone: true,
  imports: [
    ContainerComponent,
    IconContainerComponent,
    TroopersBlockComponent,
    TrooperSkillsComponent,
    TrooperChoiceComponent,
    CommonModule,
  ],
  templateUrl: './view-upgrade.component.html',
  styleUrl: './view-upgrade.component.scss',
})
export class ViewUpgradeComponent {
  public prefixs = PrefixArmy;
  public selectedTrooper: Trooper | undefined = undefined;
  public displaySkill: boolean = true;
  private destroyed$: Subject<void> = new Subject();
  public authService = inject(AuthService);
  private router = inject(Router);

  constructor() {
    this.onNav();

    this.router.events
      .pipe(
        filter((event) => event && event instanceof NavigationStart),
        takeUntil(this.destroyed$)
      )
      .subscribe(() => {
        this.onNav();
      });
  }

  onNav() {
    const navigation = this.router.getCurrentNavigation();
    let trooperId: string | undefined = undefined;
    if (navigation && navigation.extras && navigation.extras.state) {
      trooperId = navigation.extras.state['trooper'];
    }
    this.init(trooperId);
  }

  init(trooperId?: string | undefined) {
    this.reset();

    if (trooperId && this.authService.user?.troopers.length) {
      const f = this.authService.user.troopers.find((x) => x.id == trooperId);
      if (f) {
        this.selectedTrooper = f;
      }
    }

    if (!this.selectedTrooper && this.authService.user?.troopers.length) {
      this.selectedTrooper = this.authService.user.troopers[0];
    }
  }

  switchButton() {
    this.displaySkill = false;
  }

  onReturn() {
    this.displaySkill = true;
  }

  updatedTrooper(updatedTrooper: Trooper) {
    this.selectedTrooper = updatedTrooper;
  }

  reset() {
    this.selectedTrooper = undefined;
    this.displaySkill = true;
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  // on change formgroup debounce update ws
}
