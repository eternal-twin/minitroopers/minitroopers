import { CommonModule } from '@angular/common';
import { Component, inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Fight, User } from '@minitroopers/prisma';
import { PartialUserExtended, UserExtended } from '@minitroopers/shared';
import { take } from 'rxjs';
import { ArmyNameComponent } from 'src/app/components/containers/army-name/army-name.component';
import { ContainerComponent } from 'src/app/layouts/container/container.component';
import { AuthService } from 'src/app/services/auth.service';
import { FightService } from 'src/app/services/fight.service';

@Component({
  selector: 'app-view-fight',
  standalone: true,
  imports: [ContainerComponent, ArmyNameComponent, CommonModule],
  templateUrl: './view-fight.component.html',
  styleUrl: './view-fight.component.scss',
})
export class ViewFightComponent {
  private router = inject(Router);
  private authService = inject(AuthService);
  private fightService = inject(FightService);

  fight: Fight | undefined = undefined;
  loadingFight: boolean = false;
  fightSteps: any = [];

  userArmy: PartialUserExtended | undefined = undefined;
  userOpponent: PartialUserExtended | undefined = undefined;

  constructor() {
    this.loadingFight = true;
    const navigation = this.router.getCurrentNavigation();

    if (navigation && navigation.extras && navigation.extras.state) {
      this.fight = navigation.extras.state['fight'] as Fight;
      this.fightService
        .getFightDetails(this.fight.id)
        .pipe(take(1))
        .subscribe((response) => {
          this.fightSteps = response.steps;
          this.loadingFight = false;
        });
    }
  }

  onReturn(event: MouseEvent) {
    if (this.authService.user) {
      this.router.navigate(['/' + this.authService.user.armyName]);
    } else {
      this.router.navigate(['/']);
    }
  }
}
