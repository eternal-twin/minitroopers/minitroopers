export const checkNameValide = (name: string | undefined) => {
  if (
    !name ||
    typeof name !== "string" ||
    name.length < 4 ||
    name.length > 22 ||
    !name.match(/^[a-zA-Z ]*$/)
  ) {
    return false;
  }

  // route name
  if (
    [
      "oauth",
      "war",
      "admin",
      "api",
      "undefined",
      "null",
      "false",
      "true",
    ].includes(name)
  ) {
    return false;
  }

  if (hasForbiddenWord(name)) {
    return false;
  }

  return true;
};

const forbiddenName: string[] = ["toto", "toto"];
const hasForbiddenWord = (name: string) => {
  return forbiddenName.some((x) => name.includes(x));
};
